from tkinter import *
import random
import argparse
from functools import partial



def parseArgs():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--rows', required=False, default=20, type=int, help='Nombre de lignes')
    parser.add_argument('--cols', required=False, default=30 ,type=int, help='Nombre de colonne')
    parser.add_argument('--cell_size', default=10, required=False, type=int, help='taille des cases')
    parser.add_argument('--afforestation', required=False, type=float, help='taux de boisement')
    parser.add_argument('--method',required=False,type=bool,help='')
    return parser.parse_args()


def initialisation_canvas(canvas, args):


    liste_nature = []
    for i in range(args.rows):
        for j in range(args.cols):
            x = i*args.cell_size
            y = j*args.cell_size
            
            if  random.random() < 0.6:
                nature = "green"
            else:
                nature = "white"
            liste_nature.append(canevace.create_rectangle( x, y, x+args.cell_size, y+args.cell_size, fill=nature))

    return liste_nature



def regen():

    initialisation_canvas(canevace, args)



def allumerlefeu(liste_nature, index):
    liste_nature[index] = "red"




def IsArbre(liste_nature, index):
    if(liste_nature[index] == "green"):
        return True
    else:
        return False

def IsFeu(liste_nature, index):
    if(liste_nature[index] == "red"):
        return True
    else:
        return False

def callbackFeu(event):
    canvas = event.widget
    item = canvas.find_closest(event.x,event.y)
    canvas.itemconfig(item[0],fill="red")



def jadorelefeu1(liste_nature):

    args = parseArgs()
    for i in range (len(liste_nature)):
        if i == "red":

            #droite
            if(i < len(liste_nature) - 1):
                if ((i + 1) % args.cols != 0):
                    if(IsArbre(liste_nature, i + 1)):
                        allumerlefeu(liste_nature, i + 1)

            #gauche
            if(i > 0):
                if(i % args.cols != 0):
                    if(IsArbre(liste_nature, i - 1)):
                        allumerlefeu(liste_nature, i - 1)

            #haut
            if(i >= args.cols):
                if(IsArbre(liste_nature, i - args.cols)):
                    allumerlefeu(liste_nature, i - args.cols)

            #bas
            if(i <= len(liste_nature) - args.cols - 1):
                if(IsArbre(liste_nature, i + args.cols)):
                    allumerlefeu(liste_nature, i + args.cols)
        
        if i == "red":
            i = "gray"

    canevace.after(500, jadorelefeu1)

def jadorelefeu2(liste_nature):

    args = parseArgs()

    for i in range (len(liste_nature)):
        if i == "green":

            #droite
            if(i < len(liste_nature) - 1):
                if ((i + 1) % args.cols != 0):
                    if(IsFeu(liste_nature, i +1)):
                        nb_voisin = 1

            #gauche
            if(i > 0):
                if(i % args.cols != 0):
                    if(IsFeu(liste_nature, i - 1)):
                        nb_voisin = nb_voisin + 1 

            #haut
            if(i >= args.cols):
                if(IsFeu(liste_nature, i - args.cols)):
                    nb_voisin = nb_voisin + 1

            #bas
            if(i <= len(liste_nature) - args.cols - 1):
                if(IsFeu(liste_nature, i + args.cols)):
                    nb_voisin = nb_voisin + 1


            if(nb_voisin > 0 ):
                compteur = 1-(1/(nb_voisin+1))
                if random.random() > compteur :
                    allumerlefeu(liste_nature, i)
        
        if i == "red":
            i = "gray"

    canevace.after(500,jadorelefeu2)

def appelregle1():
    jadorelefeu1(liste_nature)

def appelregle2():
    jadorelefeu2(liste_nature)






args = parseArgs()

fenetre = Tk()
fenetre.title("Simulation de feu de foret")

canevace = Canvas(fenetre, 
                    width=args.cols*args.cell_size, 
                    height=args.rows*args.cell_size)


canevace.rowconfigure(0, weight=1)
canevace.rowconfigure(1, weight=1)
canevace.rowconfigure(2, weight=1)
canevace.columnconfigure(0,weight=1)
canevace.columnconfigure(1,weight=1)
canevace.columnconfigure(2,weight=1)

canevace.grid(column = 1, row = 1)



liste_nature = initialisation_canvas(canevace, args)


canevace.bind("<Button-1>", callbackFeu)

btn_regen = Button(text="Regèneration",command=regen)
btn_regen.grid(column=1,row=0)


btn_regle_1 = Button(text="Règle 1", command=appelregle1)
btn_regle_1.grid(column=1, row = 2)

btn_regle_2 = Button(text="Règle 2", command= appelregle2)
btn_regle_2.grid(column=2, row = 2)




fenetre.mainloop()

    







